/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4_2;

/**
 *
 * @author informatics
 */
public class Player {
    private String symbol;
    private int winCount;
    private int drawCount;
    private int lossCount;

    public Player(String symbol) {
        this.symbol = symbol;
        this.winCount = 0;
        this.drawCount = 0;
        this.lossCount = 0;
    }

    public String getSymbol() {
        return symbol;
    }
    
    public void win(){
        winCount++;
    }
    public void loss(){
        lossCount++;
    }
    public void draw(){
        drawCount++;
    }
    
    public int getWinCount() {
        return winCount;
    }

    public int getDrawCount() {
        return drawCount;
    }

    public int getLosscount() {
        return lossCount;
    }

    @Override
    public String toString() {
        return "Player{" + "symbol=" + symbol + ", winCount=" + winCount + ", drawCount=" + drawCount + ", lossCount=" + lossCount + '}';
    }

    

    
    
}
