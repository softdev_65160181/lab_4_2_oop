/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4_2;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Game {

    private Player player1;
    private Player player2;
    private Table table;

    public Game() {
        this.player1 = new Player("X");
        this.player2 = new Player("O");
    }

    public void newGame() {
        this.table = new Table(player1, player2);
    }

    void play() {
        showWelcome();
        newGame();
        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            if (table.checkWin()) {
                showTable();
                printWin();
                showInfo();
                newGame();
                break;
            }
            if (table.checkDraw()) {
                showTable();
                printDraw();
                showInfo();
                newGame();
                break;
            }
            table.switchPlayer();

        }

    }

    private void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    private void showTable() {
        String[][] t = table.getTable();
        for (int i=0; i<3; i++) {
            for (int j=0; j<3; j++) {
                System.out.print(t[i][j] + " ");
            }
            System.out.println("");
        }
        
    }

    private void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Please Input Number to Place : ");
            int num = sc.nextInt();
            if (table.setRowCol(num)) {
                break;
            }
        }

    }

    private void showTurn() {
        System.out.println(table.getCurrentPlayer() + " Turn");
    }

    private void printDraw() {
        System.out.println("Draw !!!");
    }

    private void printWin() {
        System.out.println(table.getCurrentPlayer() + " Win !!!");
    }

    private void showInfo() {
        System.out.print(player1);
        System.out.print(player2);
    }
}
