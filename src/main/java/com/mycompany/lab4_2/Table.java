/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4_2;

/**
 *
 * @author informatics
 */
public class Table {

    private String[][] table = {{"1", "2", "3",}, {"4", "5", "6",}, {"7", "8","9"}};
    private Player player1;
    private Player player2;
    private Player currentPlayer;
    private int turnCount = 0;
    private int row, col;

    public Table(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;
    }

    public String[][] getTable() {
        return table;
    }

    public String getCurrentPlayer() {
        return currentPlayer.getSymbol();
    }

    public boolean setRowCol(int num) {
        int col = (num-1)%3;
        int row = (num-1)/3;
        if (table[row][col].equals("X") || table[row][col].equals("O")) {
            System.out.println("Try again !!!");
            return false;
        } 
        table[row][col] = currentPlayer.getSymbol();
            return true;
        
    }

    public void switchPlayer() {
        turnCount++;
        if (currentPlayer == player1) {
            currentPlayer = player2;
        } else {
            currentPlayer = player1;
        }
    }

    public boolean checkWin() {
        saveWin();
        return checkRow(table,currentPlayer) || checkCol(table,currentPlayer) || checkDiag(table,currentPlayer) || checkAntiDiag(table,currentPlayer);
    }
    

    private boolean checkRow(String[][] table, Player currentPlayer) {
        for (int i = 0; i < 3; i++) {
            if (table[i][0].equals(currentPlayer.getSymbol()) && table[i][1].equals(currentPlayer.getSymbol()) && table[i][2].equals(currentPlayer.getSymbol())) {
                return true;
            }
        }
        return false;
    }
    
    private boolean checkCol(String[][] table, Player currentPlayer){
       for (int i=0; i<3; i++){
           if (table[0][i].equals(currentPlayer.getSymbol()) && table[1][i].equals(currentPlayer.getSymbol()) && table[2][i].equals(currentPlayer.getSymbol())) {
                return true;
            }

       }
       return false;   
    }
    
    private boolean checkDiag(String[][] table, Player currentPlayer){
        for(int i=0; i<3; i++){
            if (table[i][i] != currentPlayer.getSymbol()) {
                return false;
        }
    }
        return true;
        
    }
    
    private boolean checkAntiDiag(String[][] table, Player currentPlayer){
        for(int i=0; i<3; i++){
            if (table[i][(3-1) - i] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

        public boolean checkDraw() {
        saveDraw();
        if (turnCount == 9) {
            return true;
        }
        return false;
    }
    
    private void saveWin(){
        if(player1 == currentPlayer){
            player1.win();
            player2.loss();
        }else{
            player2.win();
            player1.loss();
        }
    }
    
    private void saveDraw(){
        player1.draw();
        player2.draw();
    }


}
